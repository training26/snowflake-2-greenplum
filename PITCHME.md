# Snowflake to Greenplum]

[Pumping data from Snowflake to Greenplum](https://pricefx.atlassian.net/wiki/spaces/IMDEV/pages/1867284878/Unload+data+from+Snowflake+and+load+them+into+Greenplum)

#HSLIDE

## Snowflake to Greenplum

- Why?
- Snowflake
- Greenplum 
- Pumping data

#HSLIDE

## Why?

- Customer Watsco stores data in Snowflake.
- Lot of data!

#HSLIDE

## Snowflake

- Cloud-based data-warehouse
- Backed by Oracle DBs
- [Web console](https://wso.us-east-1.snowflakecomputing.com/console#/monitoring/queries)

#HSLIDE

## Snowflake - data unload

- SELECT * FROM
![alt text](https://pics.me.me/thumb_ha-ha-rmermegenerator-net-ha-ha-nelson-haha-meme-52821838.png "Ha ha")

#HSLIDE

## Snowflake - data unload

Using internal stage - download CSVs (gziped)
![Using stage](https://docs.snowflake.net/manuals/_images/data-unload-snowflake.png "Internal stage")


#HSLIDE

## Greenplum

Greenplum Database is a massively parallel processing (MPP) database server with an architecture specially designed to manage large-scale analytic data warehouses.
Based on PostgreSQL

#HSLIDE

## Greenplum - loading data

- gpload binary load data from CSV files (gzipped)
- If provide multiple files - distributed load
- [Control file](http://gpdb.docs.pivotal.io/530/utility_guide/admin_utilities/gpload.html)

#HSLIDE

## Greenplum - pfx-gp component

* Generate control file for data load
* Uploads the control file to Greenplum server
* Execute gpload binary on the Greenplum server. 

[pfx-gp docs](https://pricefx.atlassian.net/wiki/spaces/IMDEV/pages/1868693528/pfx-gp+Component)

#HSLIDE

## Pumping data

* Unload data from Snowflake table into multiple gzipped CSV files located on internal Snowflake stage and downloading them into the integration server.
* Copy the data files from the integration server to Greenplum server via SFTP.
* Load data from data files into Greenplum database table.
* Cleanup data files on Snowflake stage

[Documented](https://pricefx.atlassian.net/wiki/spaces/IMDEV/pages/1867284878/Unload+data+from+Snowflake+and+load+them+into+Greenplum)

#HSLIDE
## Q&A
